import environment from "./environments";

if(process.env.NODE_ENV !== 'production'){
    const env = environment;
}

export const SECRET_KEY = process.env.SECRET || 'MeinWellundMeineStrand';

export enum COLLECTIONS{
    USERS='users',
    GENRES = 'genres',
    TAGS = 'tags',
    SHOP_PRODUCT = 'products_platforms',
    PRODUCTS = 'products',
    PLATFORMS = 'platforms'
}

export enum ACTIVE_VALUES_FILTER {
    ALL = 'ALL',
    INACTIVE = 'INACTIVE',
    ACTIVE = 'ACTIVE'
  }

export enum MESSAGES{
    TOKEN_VERIFICATION_FAILED = "Token no valido, inicia de nuevo"
}

/*
H: Horas
M: Minutos
D: Dias
*/

export enum EXPIRETIME{
    H1 = 60 * 60, //1 Hora
    H24 = 24 * H1,
    M15 = H1 / 4
}

export enum SUBSCRIPTIONS_EVENT {
    UPDATE_STOCK_PRODUCT = 'UPDATE_STOCK_PRODUCT'
  }