import GMR from 'graphql-merge-resolvers';
import resolversMailMutation from './email';

import resolversGenreMutation from './genre';
import resolversTagMutation from './tag';
import resolversUserMutation from './user';
import resolverUserMutation from './user';


const mutationResolvers = GMR.merge([
    resolversUserMutation,
    resolversGenreMutation,
    resolverUserMutation,
    resolversMailMutation,
    resolversTagMutation
]);

export default mutationResolvers;