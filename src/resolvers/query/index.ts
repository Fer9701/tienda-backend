import GMR from 'graphql-merge-resolvers';
import resolversGenreQuery from './genre';
import resolversShopProductsQuery from './shop-product';
import resolversTagQuery from './tag';
import resolverUserQuery from './user';


const queryResolvers = GMR.merge([
    resolverUserQuery,
    resolversGenreQuery,
    resolversTagQuery,
    resolversShopProductsQuery
]);

export default queryResolvers;