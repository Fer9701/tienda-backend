import slugify from "slugify";
import { COLLECTIONS } from "../config/constant";
import { IContextData } from "../interfaces/context-data.interface";
import { asignDocumentId, findOneElement } from "../lib/db-operations";
import ResolversOperationsService from "./resolvers-operations.services";

class GenresService extends ResolversOperationsService {
    collection = COLLECTIONS.GENRES;

        constructor(root: object, variables: object, context:IContextData) {
        super(root, variables, context);
        }
    async items() {
        const page = this.getVariables().pagination?.page;
        const itemsPage = this.getVariables().pagination?.itemsPage;
        console.log(this.getVariables().pagination);
        console.log(page, itemsPage);
        const result = await this.list(this.collection, 'géneros', page, itemsPage);
        return { info: result.info, status: result.status, message: result.message, genres: result.items };
        }
    async details() {
        const result = await this.get(this.collection);
        return { status: result.status, message: result.message, genre: result.item };
        }

    async insert() {
        const genre = this.getVariables().genre;
         // Comprobar que no está en blanco ni es indefinido
         if (!this.checkData( genre ||'')) {
            return {
                status: false,
                message: 'El género no se ha especificado correctamente',
                genre: null
            };
        }

        // Comprobar que no existe
        if (await this.checkInDatabase(genre || '')) {
            return {
                status: false,
                message: 'El género ya existe',
                genre: null
            };
        }

         // Si valida las opciones anteriores, venir aquí y crear el documento
         const genreObject = {
            id: await asignDocumentId(this.getDb(), this.collection, { id: -1}),
            name: genre,
            slug: slugify(genre || '', { lower: true })
        };

        const result = await this.add(this.collection, genreObject, "genero");
        return { status: result.status, message: result.message, genre: result.item };
        }

    private checkData(value: string) {
          return (value === '' || value === undefined) ? false: true;
        }

    private async checkInDatabase(value: string) {
            return await findOneElement(this.getDb(), this.collection, {
                name: value
            });
        }

    async modify() {
            const id = this.getVariables().id;
            const genre = this.getVariables().genre;
            if (!this.checkData(String(id) || '')) {
                return {
                    status: false,
                    message: 'El ID del género no se ha especificado correctamente',
                    genre: null
                };
            }
            if (!this.checkData(genre || '')) {
                return {
                    status: false,
                    message: 'El género no se ha especificado correctamente',
                    genre: null
                };
            }
            const objectUpdate = { 
                name: genre,
                slug: slugify(genre || '', {lower: true})
            };
            
            const result = await this.update(this.collection, { id }, objectUpdate, 'genero');
            return { status: result.status, message: result.message, genre: result.item };
        }

    async delete() {
            const id = this.getVariables().id;
            if (!this.checkData(String(id) || '')) {
                return {
                    status: false,
                    message: 'El ID del género no se ha especificado correctamente',
                    genre: null
                };
            }
            const result = await this.del(this.collection, { id }, 'genero');
            return { status: result.status, message: result.message };
        }

    async block() {
            const id = this.getVariables().id;
            if (!this.checkData(String(id) || '')) {
                return {
                    status: false,
                    message: 'El ID del género no se ha especificado correctamente',
                    genre: null
                };
            }
            const result = await this.update(this.collection, { id }, { active: false}, 'genero');
            return {
                status: result.status,
                message: (result.status) ? `Borrado Correctamente`: `No se ha podido bloquear`
              };
            };
        }
    
    

export default GenresService;